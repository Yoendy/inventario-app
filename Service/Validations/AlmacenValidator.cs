using Service.Dto;
using FluentValidation;

namespace Service.Validations
{
    public class AlmacenValidator : AbstractValidator<AlmacenDto>
    {
        public AlmacenValidator()
        {
            RuleFor(item => item.Descripcion).NotEmpty().WithMessage("La descripción es obligatoria.");

            RuleFor(item => item.Nombre).NotEmpty().WithMessage("El nombre es obligatorio.")
                .MaximumLength(150).WithMessage("El nombre no puede tener mas de 150 caracteres.");
        }
    }
}