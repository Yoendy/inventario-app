using System;
using System.Collections.Generic;
using System.Text;
using DomainModel;

namespace Service.Dto
{
    public class ArticuloDto : BaseModel
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int Existencia { get; set; } 
        public int TipoInventarioId { get; set; }
        public TipoInventarioDto TipoInventario { get; set; }
        public Decimal CostoUnitario { get; set; }
    }
}