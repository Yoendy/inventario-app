using Service.Dto;
using FluentValidation;

namespace Service.Validations
{
    public class ArticuloValidator : AbstractValidator<ArticuloDto>
    {
        public ArticuloValidator()
        {
            RuleFor(item => item.Descripcion).NotEmpty().WithMessage("La descripcion es obligatoria.");

            RuleFor(item => item.Nombre).NotEmpty().WithMessage("El nombre es obligatorio.")
                .MaximumLength(150).WithMessage("El nombre no puede tener mas de 150 caracteres.");

            RuleFor(item => item.Existencia).NotEmpty().WithMessage("La existencia es obligatoria.")
                .GreaterThan(0).WithMessage("La existencia debe ser mayor que 0.");

            RuleFor(item => item.CostoUnitario).NotEmpty().WithMessage("El costo unitario es obligatorio.")
            .GreaterThan(0).WithMessage("El costo unitario debe ser mayor que 0.");
        }
    }
}