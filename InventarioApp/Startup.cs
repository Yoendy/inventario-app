﻿using AutoMapper;
using DataAccess;
using DomainModel;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repository;
using Service;
using Service.Dto;
using FluentValidation;
using Service.Validations;

namespace InventarioApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Add SQL Server support
            //services.AddDbContext<CustomersDbContext>(context =>
            //{
            //    context.UseSqlServer(Configuration.GetConnectionString("SqlServerConnectionString"));
            //});

            //Add SqLite support
            services.AddDbContext<InventarioContext>(context =>
            {
                context.UseSqlite(Configuration.GetConnectionString("SqliteConnectionString"));
            });

            // Add framework services.
            //services.AddAutoMapper();
            services.AddMvc();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAnyOrigin",
                    builder => builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                );
            });

            Mapper.Initialize(config =>
            {
                config.CreateMissingTypeMaps = true;
            });

            /*TIPO INVENTARIO*/
            services.AddScoped<IGenericRepository<TipoInventario>, GenericRepository<TipoInventario>>();
            services.AddScoped<IGenericService<TipoInventarioDto>, BaseService<TipoInventarioDto, TipoInventario>>();
            services.AddScoped<AbstractValidator<TipoInventarioDto>, TipoInventarioValidator>();

            /*ALMACEN*/
            services.AddScoped<IGenericRepository<Almacen>, GenericRepository<Almacen>>();
            services.AddScoped<IGenericService<AlmacenDto>, BaseService<AlmacenDto, Almacen>>();
            services.AddScoped<AbstractValidator<AlmacenDto>, AlmacenValidator>();

            /*TIPO TRANSACCION*/
            services.AddScoped<IGenericRepository<TipoTransaccion>, GenericRepository<TipoTransaccion>>();
            services.AddScoped<IGenericService<TipoTransaccionDto>, BaseService<TipoTransaccionDto, TipoTransaccion>>();
            services.AddScoped<AbstractValidator<TipoTransaccionDto>, TipoTransaccionValidator>();

            /*ARTICULO*/
            services.AddScoped<IGenericRepository<Articulo>, GenericRepository<Articulo>>();
            services.AddScoped<IGenericService<ArticuloDto>, BaseService<ArticuloDto, Articulo>>();
            services.AddScoped<AbstractValidator<ArticuloDto>, ArticuloValidator>();

            /*TRANSACCION*/
            services.AddScoped<IGenericRepository<Transaccion>, GenericRepository<Transaccion>>();
            services.AddScoped<IGenericService<TransaccionDto>, BaseService<TransaccionDto, Transaccion>>();
            services.AddScoped<AbstractValidator<TransaccionDto>, TransaccionValidator>();
            //services.AddScoped<IGenericService<TipoInventarioDto>, BaseService<TipoInventarioDto,TipoInventario>();
            //services.AddTransient<CustomersDbSeeder>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(
                options => options.WithOrigins("http://localhost:8080")
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
            );
            app.UseMvc();
        }
    }
}
