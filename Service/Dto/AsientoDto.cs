using System;
using System.Collections.Generic;
using System.Text;
using DomainModel;

namespace Service.Dto
{
    public class AsientoDto 
    {
        public string Auxiliar { get; set; } = "4";
        public string Descripcion { get; set; } = "Asiento de Inventarios correspondiente al periodo"+DateTime.Now.Year+"-"+DateTime.Now.Month;
        public List<DetalleAsientoDto > Detalles{get;set;} = new List<DetalleAsientoDto>();
    }
}