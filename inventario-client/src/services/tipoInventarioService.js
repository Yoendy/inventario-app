import Vue from 'vue';
import axios from 'axios';
import swal from 'sweetalert';

let config = {
    headers: {
        'Access-Control-Allow-Origin': "*",
    }
}

var TipoInventario = {
    name: 'TipoInventarioService',
    data: function() {
        return {
            baseUrl: "http://localhost:5000/api/TipoInventario/",
            TipoInventarioes: [],
            TipoInventario: {
                Id: 0,
                CuentaContable: '',
                Descripcion: '',
                Activo: true
            },
            searchKey: '',
            guardardo: false,
            readOnly: false
        }
    },
    methods: {
        guardar: function() {
            console.log(this.TipoInventario);

            var model = {
                id: this.TipoInventario.Id,
                cuentaContable: this.TipoInventario.CuentaContable,
                descripcion: this.TipoInventario.Descripcion,
                activo: this.TipoInventario.Activo
            };

            console.log(model);

            axios.post(this.baseUrl, model)
                .then(response => {
                    // JSON responses are automatically parsed.
                    console.log(response.data);
                    this.TipoInventario.Id = response.data.Id;
                    //noinspection JSAnnotator
                    this.guardardo = true;
                    //Notification
                    this.showMessage("Los datos se guardaron correctamente.", "", "success");

                })
                .catch(e => {
                    console.log(e);
                    //Notification
                    this.showMessage(e, "", "error");
                });
        },
        getOne: function(id) {
            var self = this;
            if (id != undefined) {
                axios.get(this.baseUrl + id)
                    .then(response => {
                        // JSON responses are automatically parsed.
                        console.log(response.data)
                            //noinspection JSAnnotator
                            //self.TipoInventario = response.data;
                        self.TipoInventario.Id = response.data.id;
                        self.TipoInventario.CuentaContable = response.data.cuentaContable;
                        self.TipoInventario.Descripcion = response.data.descripcion;
                        self.TipoInventario.Activo = response.data.activo;
                    })
                    .catch(e => {
                        console.log(e);
                        this.showMessage(e, "", "error");
                    });
            }
        },
        eliminar: function(id) {
            var self = this;
            if (id != undefined) {
                swal({
                    title: "Esta seguro que desea eliminar este tipo de inventario?",
                    text: "Esta acción es irreversible!",
                    icon: "warning",
                    showCancelButton: true,
                    closeOnConfirm: true,
                    closeOnCancel: false
                }).then((willDelete) => {
                    if (willDelete) {
                        axios.delete(this.baseUrl + id)
                            .then(response => {
                                this.showMessage("Los datos se eliminaron correctamente.", "", "success");
                                console.log(response.data);
                            })
                            .catch(e => {
                                console.log(e);
                                this.showMessage(e, "", "error");
                            });
                    } else {
                        this.showMessage("Los datos estan seguros.", "", "success");
                    }
                });;

            }
        },
        getAll: function() {
            var self = this;
            axios.get(this.baseUrl, {}, config)
                .then(response => {
                    // JSON responses are automatically parsed.
                    console.log(response.data);
                    //noinspection JSAnnotator
                    self.TipoInventarioes = response.data;
                })
                .catch(e => {
                    console.log(e);
                    this.showMessage(e, "", "error");
                });
        },
        modificar: function() {
            console.log(this.TipoInventario);
            axios.put(this.baseUrl + this.TipoInventario.Id, {
                    id: this.TipoInventario.Id,
                    cuentaContable: this.TipoInventario.CuentaContable,
                    descripcion: this.TipoInventario.Descripcion,
                    activo: this.TipoInventario.Activo
                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    console.log(response.data)
                        //Notification
                    this.showMessage("Los datos se modificaron correctamente.", "", "success");

                })
                .catch(e => {
                    console.log(e);

                    this.showMessage(e, "", "error");
                });


        },
        validate: function() {
            this.$validator.validateAll();

            if (!this.errors.any()) {
                this.TipoInventario.Id != 0 ? this.modificar() : this.guardar();
            }
        },
        showMessage: function(titulo, mensaje, tipo) {
            //Notification
            swal({
                title: titulo,
                text: mensaje,
                icon: tipo
            });
        }
    },
    computed: {
        filteredList: function() {
            return this.TipoInventarioes.filter(function(item) {
                return this.searchKey == '' || (item.cuentaContable.toLowerCase().indexOf(this.searchKey.toLowerCase())) !== -1 || (item.descripcion.toLowerCase().indexOf(this.searchKey.toLowerCase())) !== -1;
            }, this);
        }
    }

};

export default TipoInventario;