using Service.Dto;
using FluentValidation;

namespace Service.Validations
{
    public class TipoInventarioValidator : AbstractValidator<TipoInventarioDto>
    {
        public  TipoInventarioValidator()
        {
            RuleFor(item => item.Descripcion).NotEmpty().WithMessage("La descripción es obligatoria.");

            RuleFor(item => item.CuentaContable).NotEmpty().WithMessage("La cuenta contable es obligatoria.");
                
        }
    }
}