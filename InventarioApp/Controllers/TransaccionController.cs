using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Repository;
using RestSharp;
using Service;
using Service.Dto;

namespace InventarioApp.Controllers
{
    [Route("api/[controller]")]
    public class TransaccionController : BaseController<IGenericService<TransaccionDto>, TransaccionDto, Transaccion>
    {
         ILogger _Logger;
         string BASE_URL = "";
        public TransaccionController(IGenericService<TransaccionDto> service, ILoggerFactory loggerFactory) : base(service, loggerFactory)
        {
            _Logger = loggerFactory.CreateLogger(nameof(TransaccionController));
        }
        
        [HttpPost]
        public async Task<IActionResult> EnviarContabilidad(IEnumerable<Transaccion> transacciones)
        {
            try
            {
                if(!transacciones.Any()) throw new Exception("No hay transacciones en este periodo");
                
                var asiento = new AsientoDto();
                //AGREGAR DEBITO
                asiento.Detalles.Add(new DetalleAsientoDto {CuentaId = "6",Descripcion = "Inventario", TipoMovimiento="DB",Monto = transacciones.Sum(x =>x.Monto)});
                //AGREGAR CREDITO
                 asiento.Detalles.Add(new DetalleAsientoDto {CuentaId = "82",Descripcion = "Cuentas x Pagar Proveedor X", TipoMovimiento="CR",Monto = transacciones.Sum(x =>x.Monto)});
                //RESTSHARP
                var client = new RestClient(BASE_URL);
                var request = new RestRequest(Method.POST);
                request.AddBody(asiento);

                TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();

                RestRequestAsyncHandle handle = client.ExecuteAsync(request, r => taskCompletion.SetResult(r));

                RestResponse response = (RestResponse)(await taskCompletion.Task);

                //FORMAR OBJECTO DE CONTABILIDAD
                return Ok(response);
            }
            catch(Exception ex)
            {
                _Logger.LogError(ex.Message);
                return BadRequest(ex);
                
            }
        }

        
    }
}
