using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel
{
    public class Transaccion : BaseModel
    {
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; } = DateTime.Now;
        public int Cantidad { get; set; }
        public Decimal Monto {get; set;}
        public bool Enviado { get; set; } = false;
        public int TipoTransaccionId { get; set; } 
        public TipoTransaccion TipoTransaccion { get; set; }
    }
}
