using DomainModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Mappings
{
    public class ArticuloMap : IEntityTypeConfiguration<Articulo>
    {
        public void Configure(EntityTypeBuilder<Articulo> builder)
        {
            builder.Property(m => m.Descripcion).IsRequired();
            builder.Property(m => m.Nombre).IsRequired().HasMaxLength(150);
            builder.Property(m=> m.Existencia).IsRequired().HasDefaultValue(0);
            builder.Property(m=> m.CostoUnitario).IsRequired().HasDefaultValue(0.00);
           
        }
    }
}
