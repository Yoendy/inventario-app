﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DataAccess.Migrations
{
    public partial class AgregarTransaccion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.AlterColumn<string>(
            //     name: "Nombre",
            //     table: "Articulos",
            //     maxLength: 150,
            //     nullable: false,
            //     oldClrType: typeof(string),
            //     oldNullable: true);

            // migrationBuilder.AlterColumn<int>(
            //     name: "Existencia",
            //     table: "Articulos",
            //     nullable: false,
            //     defaultValue: 0,
            //     oldClrType: typeof(int))
            //     .Annotation("Sqlite:Autoincrement", true);

            // migrationBuilder.AlterColumn<string>(
            //     name: "Descripcion",
            //     table: "Articulos",
            //     nullable: false,
            //     oldClrType: typeof(string),
            //     oldNullable: true);

            // migrationBuilder.AlterColumn<decimal>(
            //     name: "CostoUnitario",
            //     table: "Articulos",
            //     nullable: false,
            //     defaultValue: 0m,
            //     oldClrType: typeof(decimal));

            migrationBuilder.CreateTable(
                name: "Transacciones",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Activo = table.Column<bool>(nullable: false),
                    Cantidad = table.Column<int>(nullable: false, defaultValue: 0),
                        
                    CreadoPor = table.Column<string>(nullable: true),
                    Descripcion = table.Column<string>(nullable: false),
                    Enviado = table.Column<bool>(nullable: false),
                    Fecha = table.Column<DateTime>(nullable: false),
                    FechaCreacion = table.Column<DateTime>(nullable: false),
                    FechaModificacion = table.Column<DateTime>(nullable: true),
                    ModificadoPor = table.Column<string>(nullable: true),
                    Monto = table.Column<decimal>(nullable: false, defaultValue: 0m),
                    TipoTransaccionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transacciones", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Transacciones_TiposTransaccion_TipoTransaccionId",
                        column: x => x.TipoTransaccionId,
                        principalTable: "TiposTransaccion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Transacciones_TipoTransaccionId",
                table: "Transacciones",
                column: "TipoTransaccionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.DropTable(
            //     name: "Transacciones");

            // migrationBuilder.AlterColumn<string>(
            //     name: "Nombre",
            //     table: "Articulos",
            //     nullable: true,
            //     oldClrType: typeof(string),
            //     oldMaxLength: 150);

            // migrationBuilder.AlterColumn<int>(
            //     name: "Existencia",
            //     table: "Articulos",
            //     nullable: false,
            //     oldClrType: typeof(int),
            //     oldDefaultValue: 0)
            //     .OldAnnotation("Sqlite:Autoincrement", true);

            // migrationBuilder.AlterColumn<string>(
            //     name: "Descripcion",
            //     table: "Articulos",
            //     nullable: true,
            //     oldClrType: typeof(string));

            // migrationBuilder.AlterColumn<decimal>(
            //     name: "CostoUnitario",
            //     table: "Articulos",
            //     nullable: false,
            //     oldClrType: typeof(decimal),
            //     oldDefaultValue: 0m);
        }
    }
}
