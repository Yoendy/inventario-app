﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DataAccess.Migrations
{
    public partial class AgregarArticulos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.DropPrimaryKey(
            //     name: "PK_TipoTransaccion",
            //     table: "TipoTransaccion");

            // migrationBuilder.DropPrimaryKey(
            //     name: "PK_Almacen",
            //     table: "Almacen");

            migrationBuilder.RenameTable(
                name: "TipoTransaccion",
                newName: "TiposTransaccion");

            migrationBuilder.RenameTable(
                name: "Almacen",
                newName: "Almacenes");

            // migrationBuilder.AddPrimaryKey(
            //     name: "PK_TiposTransaccion",
            //     table: "TiposTransaccion",
            //     column: "Id");

            // migrationBuilder.AddPrimaryKey(
            //     name: "PK_Almacenes",
            //     table: "Almacenes",
            //     column: "Id");

            migrationBuilder.CreateTable(
                name: "Articulos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Activo = table.Column<bool>(nullable: false),
                    CostoUnitario = table.Column<decimal>(nullable: false),
                    CreadoPor = table.Column<string>(nullable: true),
                    Descripcion = table.Column<string>(nullable: true),
                    Existencia = table.Column<int>(nullable: false),
                    FechaCreacion = table.Column<DateTime>(nullable: false),
                    FechaModificacion = table.Column<DateTime>(nullable: true),
                    ModificadoPor = table.Column<string>(nullable: true),
                    Nombre = table.Column<string>(nullable: true),
                    TipoInventarioId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Articulos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Articulos_TiposInventario_TipoInventarioId",
                        column: x => x.TipoInventarioId,
                        principalTable: "TiposInventario",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Articulos_TipoInventarioId",
                table: "Articulos",
                column: "TipoInventarioId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Articulos");

            // migrationBuilder.DropPrimaryKey(
            //     name: "PK_TiposTransaccion",
            //     table: "TiposTransaccion");

            // migrationBuilder.DropPrimaryKey(
            //     name: "PK_Almacenes",
            //     table: "Almacenes");

            migrationBuilder.RenameTable(
                name: "TiposTransaccion",
                newName: "TipoTransaccion");

            migrationBuilder.RenameTable(
                name: "Almacenes",
                newName: "Almacen");

            // migrationBuilder.AddPrimaryKey(
            //     name: "PK_TipoTransaccion",
            //     table: "TipoTransaccion",
            //     column: "Id");

            // migrationBuilder.AddPrimaryKey(
            //     name: "PK_Almacen",
            //     table: "Almacen",
            //     column: "Id");
        }
    }
}
