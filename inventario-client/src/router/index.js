import Vue from 'vue';
import Router from 'vue-router';
import Vue2Filters from 'vue2-filters';


import HelloWorld from '@/components/HelloWorld';
import PageNotFound from '@/components/PageNotFound';

import AlmacenList from '@/components/AlmacenList';
import AlmacenAdd from '@/components/AlmacenAdd';

import TipoInventarioList from '@/components/TipoInventarioList';
import TipoInventarioAdd from '@/components/TipoInventarioAdd';

import TipoTransaccionList from '@/components/TipoTransaccionList';
import TipoTransaccionAdd from '@/components/TipoTransaccionAdd';

import ArticuloList from '@/components/ArticuloList';
import ArticuloAdd from '@/components/ArticuloAdd';

Vue.use(Router);
Vue.use(Vue2Filters);

var router = new Router({
  routes: [{
      path: '/',
      name: 'Articulos',
      component: ArticuloList
    },
    {
      path: '/Almacenes/',
      name: 'AlmacenList',
      component: AlmacenList
    },
    {
      path: '/Almacenes/Agregar',
      name: 'AlmacenAdd',
      component: AlmacenAdd
    },
    {
      path: '/Almacenes/Editar/:id',
      name: 'AlmacenEdit',
      component: AlmacenAdd
    },
    {
      path: '/Almacenes/Ver/:id',
      name: 'AlmacenView',
      component: AlmacenAdd
    },
    {
      path: '/TiposInventario/',
      name: 'TipoInventarioList',
      component: TipoInventarioList
    },
    {
      path: '/TiposInventario/Agregar',
      name: 'TipoInventarioAdd',
      component: TipoInventarioAdd
    },
    {
      path: '/TiposInventario/Editar/:id',
      name: 'TipoInventarioEdit',
      component: TipoInventarioAdd
    },
    {
      path: '/TiposInventario/Ver/:id',
      name: 'TipoInventarioView',
      component: TipoInventarioAdd
    },
    {
      path: '/TiposTransaccion/',
      name: 'TipoTransaccionList',
      component: TipoTransaccionList
    },
    {
      path: '/TiposTransaccion/Agregar',
      name: 'TipoTransaccionAdd',
      component: TipoTransaccionAdd
    },
    {
      path: '/TiposTransaccion/Editar/:id',
      name: 'TipoTransaccionEdit',
      component: TipoTransaccionAdd
    },
    {
      path: '/TiposTransaccion/Ver/:id',
      name: 'TipoTransaccionView',
      component: TipoTransaccionAdd
    },
    {
      path: '/Articulos/',
      name: 'ArticuloList',
      component: ArticuloList
    },
    {
      path: '/Articulos/Agregar',
      name: 'ArticuloAdd',
      component: ArticuloAdd
    },
    {
      path: '/Articulos/Editar/:id',
      name: 'ArticuloEdit',
      component: ArticuloAdd
    },
    {
      path: '/Articulos/Ver/:id',
      name: 'ArticuloView',
      component: ArticuloAdd
    },
    {
      path: '*',
      name: "PageNotFound",
      component: PageNotFound
    }
  ]
});



export default router;
