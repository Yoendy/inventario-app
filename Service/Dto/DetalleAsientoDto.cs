using System;
using System.Collections.Generic;
using System.Text;
using DomainModel;

namespace Service.Dto
{
    public class DetalleAsientoDto 
    {
        public string CuentaId { get; set; }
        public string Descripcion { get; set; }
        public string TipoMovimiento { get; set; }
        public Decimal Monto {get; set;}
        
    }
}