using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel
{
    public class Articulo : BaseModel
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int Existencia { get; set; } 
        public int TipoInventarioId { get; set; }
        public TipoInventario TipoInventario { get; set; }
        public Decimal CostoUnitario { get; set; }
    }
}
