﻿using DomainModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Mappings
{
    public class TipoInventarioMap : IEntityTypeConfiguration<TipoInventario>
    {
        public void Configure(EntityTypeBuilder<TipoInventario> builder)
        {
            builder.Property(m => m.Descripcion).IsRequired().HasMaxLength(50);
            builder.Property(m => m.CuentaContable).IsRequired();
           
        }
    }
}
