using System;
using System.Collections.Generic;
using System.Text;
using DomainModel;

namespace Service.Dto
{
    public class TransaccionDto : BaseModel
    {
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; } = DateTime.Now;
        public int Cantidad { get; set; }
        public Decimal Monto {get; set;}
        public bool Enviado { get; set; } = false;
        public int TipoTransaccionId { get; set; } 
        public TipoTransaccion TipoTransaccion { get; set; }
        public int ArticuloId { get; set; } 
        public Articulo Articulo { get; set; }
        public int AlmacenId { get; set; } 
        public Almacen Almacen { get; set; }
    }
}
