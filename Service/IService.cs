﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DomainModel;

namespace Service
{
    public interface IGenericService<T>   where T : class
    {
        
        Task<ICollection<T>> GetAllAsync();
        Task<T> FindAsync(int id);

        Task<T> AddAsync(T dto);

        Task<int> DeleteAsync(T dto);

        Task<T> UpdateAsync(T dto);

        Task<T> SaveAsync(T dto);

    }
}
