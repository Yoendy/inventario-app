﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Repository;
using Service;
using Service.Dto;

namespace InventarioApp.Controllers
{
    [Route("api/[controller]")]
    public class TipoInventarioController : BaseController<IGenericService<TipoInventarioDto>, TipoInventarioDto, TipoInventario>
    {
        public TipoInventarioController(IGenericService<TipoInventarioDto> service, ILoggerFactory loggerFactory) : base(service, loggerFactory)
        {
        }
    }
}
