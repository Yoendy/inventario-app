﻿using System;

namespace DomainModel
{
    public abstract class BaseModel
    {
        public int Id { get; set; }
        public string CreadoPor { get; set; } = "System";
        public DateTime FechaCreacion { get; set; } = DateTime.Now;
        public string ModificadoPor { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public bool Activo { get; set; } = true;
    }
}
