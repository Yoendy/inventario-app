import Vue from 'vue';
import axios from 'axios';
import swal from 'sweetalert';

let config = {
    headers: {
        'Access-Control-Allow-Origin': "*",
    }
}

let serverUrl = "http://localhost:5000/api/";

var Articulo = {
    name: 'ArticuloService',
    data: function() {
        return {

            baseUrl: serverUrl + "Articulo/",
            tipoInventarioUrl: serverUrl + "TipoInventario/",
            TipoInventarios: [],
            Articuloes: [],
            Articulo: {
                Id: 0,
                Nombre: '',
                Descripcion: '',
                Existencia: 0,
                CostoUnitario: 0.00,
                TipoInventarioId: 1,
                Activo: true
            },
            searchKey: '',
            guardardo: false,
            readOnly: false
        }
    },
    methods: {
        guardar: function() {
            console.log(this.Articulo);

            var model = {
                id: this.Articulo.Id,
                nombre: this.Articulo.Nombre,
                descripcion: this.Articulo.Descripcion,
                existencia: this.Articulo.Existencia,
                costoUnitario: this.Articulo.CostoUnitario,
                tipoInventarioId: this.Articulo.TipoInventarioId,
                activo: this.Articulo.Activo
            };

            console.log(model);

            axios.post(this.baseUrl, model)
                .then(response => {
                    // JSON responses are automatically parsed.
                    console.log(response.data);
                    this.Articulo.Id = response.data.Id;
                    //noinspection JSAnnotator
                    this.guardardo = true;
                    //Notification
                    this.showMessage("Los datos se guardaron correctamente.", "", "success");

                })
                .catch(e => {
                    console.log(e);
                    //Notification
                    this.showMessage(e, "", "error");
                });
        },
        getOne: function(id) {
            var self = this;
            if (id != undefined) {


                axios.get(this.baseUrl + id)
                    .then(response => {
                        // JSON responses are automatically parsed.
                        console.log(response.data)
                            //noinspection JSAnnotator
                            //self.Articulo = response.data;
                        self.Articulo.Id = response.data.id;
                        self.Articulo.Nombre = response.data.nombre;
                        self.Articulo.Descripcion = response.data.descripcion;
                        self.Articulo.Existencia = response.data.existencia;
                        self.Articulo.CostoUnitario = response.data.costoUnitario;
                        self.Articulo.TipoInventarioId = response.data.tipoInventarioId;
                        self.Articulo.Activo = response.data.activo;
                    })
                    .catch(e => {
                        console.log(e);
                        this.showMessage(e, "", "error");
                    });
            }
        },
        eliminar: function(id) {
            var self = this;
            if (id != undefined) {
                swal({
                    title: "Esta seguro que desea eliminar este articulo?",
                    text: "Esta acción es irreversible!",
                    icon: "warning",
                    showCancelButton: true,
                    closeOnConfirm: true,
                    closeOnCancel: false
                }).then((willDelete) => {
                    if (willDelete) {
                        axios.delete(this.baseUrl + id)
                            .then(response => {
                                this.showMessage("Los datos se eliminaron correctamente.", "", "success");
                                console.log(response.data);
                            })
                            .catch(e => {
                                console.log(e);
                                this.showMessage(e, "", "error");
                            });
                    } else {
                        this.showMessage("Los datos estan seguros.", "", "success");
                    }
                });;

            }
        },
        getAll: function() {
            var self = this;
            axios.get(this.baseUrl, {}, config)
                .then(response => {
                    // JSON responses are automatically parsed.
                    console.log(response.data);
                    //noinspection JSAnnotator
                    self.Articuloes = response.data;
                })
                .catch(e => {
                    console.log(e);
                    this.showMessage(e, "", "error");
                });
        },
        getTiposInventarios: function() {
            var self = this;
            axios.get(this.tipoInventarioUrl, {}, config)
                .then(response => {
                    // JSON responses are automatically parsed.
                    console.log(response.data);
                    //noinspection JSAnnotator
                    self.TipoInventarios = response.data;
                })
                .catch(e => {
                    console.log(e);
                    this.showMessage(e, "", "error");
                });
        },
        modificar: function() {
            console.log(this.Articulo);
            axios.put(this.baseUrl + this.Articulo.Id, {
                    id: this.Articulo.Id,
                    nombre: this.Articulo.Nombre,
                    descripcion: this.Articulo.Descripcion,
                    existencia: this.Articulo.Existencia,
                    costoUnitario: this.Articulo.CostoUnitario,
                    tipoInventarioId: this.Articulo.TipoInventarioId,
                    activo: this.Articulo.Activo
                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    console.log(response.data)
                        //Notification
                    this.showMessage("Los datos se modificaron correctamente.", "", "success");

                })
                .catch(e => {
                    console.log(e);

                    this.showMessage(e, "", "error");
                });


        },
        validate: function() {
            this.$validator.validateAll();

            if (!this.errors.any()) {
                this.Articulo.Id != 0 ? this.modificar() : this.guardar();
            }
        },
        showMessage: function(titulo, mensaje, tipo) {
            //Notification
            swal({
                title: titulo,
                text: mensaje,
                icon: tipo
            });
        }
    },
    computed: {
        filteredList: function() {
            return this.Articuloes.filter(function(item) {
                return this.searchKey == '' || (item.nombre.toLowerCase().indexOf(this.searchKey.toLowerCase()) !== -1) || (item.descripcion.toLowerCase().indexOf(this.searchKey.toLowerCase())) !== -1;
            }, this);
        }
    }

};

export default Articulo;