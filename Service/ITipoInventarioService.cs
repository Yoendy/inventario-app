﻿using DataAccess;
using DomainModel;
using Repository;
using System;

namespace Service
{
    public class TipoInventarioService : GenericRepository<TipoInventario>, IGenericRepository<TipoInventario>
    {
        public TipoInventarioService(InventarioContext context) : base(context)
        {
        }
    }
}
