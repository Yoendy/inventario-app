using System;
using System.Collections.Generic;
using System.Text;
using DomainModel;

namespace Service.Dto
{
    public class AlmacenDto : BaseModel
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}