using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel
{
    public class TipoTransaccion : BaseModel
    {
        
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}
