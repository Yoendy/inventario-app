using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using DomainModel;
using DomainModel.Base;
using Repository;
using FluentValidation;

namespace Service
{
    /*T: Domain Model
     M: DTO Model,
     E: Repository*/
    public  class BaseService<TDto, TEntity> :  IGenericService<TDto>
        where TDto : class where TEntity : BaseModel
    {
        private readonly IGenericRepository<TEntity> _repository;
        private readonly AbstractValidator<TDto> _validator;

        public BaseService(IGenericRepository<TEntity> repository, AbstractValidator<TDto> validator)
        {
            //AutoMapper.Mapper.CreateMap<T, M>();
            //AutoMapper.Mapper.CreateMap<M, T>();
            // Mapper.Initialize(config =>
            // {
            //     config.CreateMissingTypeMaps = true;
            // });
            _repository = repository;
            _validator = validator;
        }

        public async Task<TDto> AddAsync(TDto dto)
        {
            var model = Mapper.Map<TEntity>(dto);
            var data = await _repository.AddAsync(model);
            return Mapper.Map<TDto>(data);
        }

        public async Task<int> DeleteAsync(TDto dto)
        {
            var model = Mapper.Map<TEntity>(dto);
            var data = await _repository.DeleteAsync(model);
            return data;
        }

        public async Task<TDto> FindAsync(int id)
        {
            var data = await _repository.FindAsync(id);
            return Mapper.Map<TDto>(data);
        }

        public  async Task<ICollection<TDto>> GetAllAsync()
        {
            var data = await _repository.GetAllAsync();
            return Mapper.Map<List<TDto>>(data);
        }

        public async Task<TDto> SaveAsync(TDto dto)
        {
            if(_validator != null) 
                _validator.ValidateAndThrow(dto);
                
            var model = Mapper.Map<TEntity>(dto);
            var exist = await FindAsync(model.Id) != null;
            if(exist)
                return await UpdateAsync(dto);
            else
            return await AddAsync(dto);
        }

        public async Task<TDto> UpdateAsync(TDto dto)
        {
            var model = Mapper.Map<TEntity>(dto);
            var data = await _repository.UpdateAsync(model, model.Id);
            return Mapper.Map<TDto>(data);
        }
    }
}
