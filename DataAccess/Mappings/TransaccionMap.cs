using DomainModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Mappings
{
    public class TransaccionMap : IEntityTypeConfiguration<Transaccion>
    {
        public void Configure(EntityTypeBuilder<Transaccion> builder)
        {
            builder.Property(m => m.Descripcion).IsRequired();
            builder.Property(m=> m.Cantidad).IsRequired().HasDefaultValue(0);
            builder.Property(m=> m.Monto).IsRequired().HasDefaultValue(0.00);
           
        }
    }
}
