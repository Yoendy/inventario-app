import Vue from 'vue';
import axios from 'axios';
import swal from 'sweetalert';

let config = {
    headers: {
        'Access-Control-Allow-Origin': "*",
    }
}

var TipoTransaccion = {
    name: 'TipoTransaccionService',
    data: function() {
        return {
            baseUrl: "http://localhost:5000/api/TipoTransaccion/",
            TipoTransacciones: [],
            TipoTransaccion: {
                Id: 0,
                Nombre: '',
                Descripcion: '',
                Activo: true
            },
            searchKey: '',
            guardardo: false,
            readOnly: false
        }
    },
    methods: {
        guardar: function() {
            console.log(this.TipoTransaccion);

            var model = {
                id: this.TipoTransaccion.Id,
                nombre: this.TipoTransaccion.Nombre,
                descripcion: this.TipoTransaccion.Descripcion,
                activo: this.TipoTransaccion.Activo
            };

            console.log(model);

            axios.post(this.baseUrl, model)
                .then(response => {
                    // JSON responses are automatically parsed.
                    console.log(response.data);
                    this.TipoTransaccion.Id = response.data.Id;
                    //noinspection JSAnnotator
                    this.guardardo = true;
                    //Notification
                    this.showMessage("Los datos se guardaron correctamente.", "", "success");

                })
                .catch(e => {
                    console.log(e);
                    //Notification
                    this.showMessage(e, "", "error");
                });
        },
        getOne: function(id) {
            var self = this;
            if (id != undefined) {
                axios.get(this.baseUrl + id)
                    .then(response => {
                        // JSON responses are automatically parsed.
                        console.log(response.data)
                            //noinspection JSAnnotator
                            //self.TipoTransaccion = response.data;
                        self.TipoTransaccion.Id = response.data.id;
                        self.TipoTransaccion.Nombre = response.data.nombre;
                        self.TipoTransaccion.Descripcion = response.data.descripcion;
                        self.TipoTransaccion.Activo = response.data.activo;
                    })
                    .catch(e => {
                        console.log(e);
                        this.showMessage(e, "", "error");
                    });
            }
        },
        eliminar: function(id) {
            var self = this;
            if (id != undefined) {
                swal({
                    title: "Esta seguro que desea eliminar este tipo de transacción?",
                    text: "Esta acción es irreversible!",
                    icon: "warning",
                    showCancelButton: true,
                    closeOnConfirm: true,
                    closeOnCancel: false
                }).then((willDelete) => {
                    if (willDelete) {
                        axios.delete(this.baseUrl + id)
                            .then(response => {
                                this.showMessage("Los datos se eliminaron correctamente.", "", "success");
                                console.log(response.data);
                            })
                            .catch(e => {
                                console.log(e);
                                this.showMessage(e, "", "error");
                            });
                    } else {
                        this.showMessage("Los datos estan seguros.", "", "success");
                    }
                });;

            }
        },
        getAll: function() {
            var self = this;
            axios.get(this.baseUrl, {}, config)
                .then(response => {
                    // JSON responses are automatically parsed.
                    console.log(response.data);
                    //noinspection JSAnnotator
                    self.TipoTransacciones = response.data;
                })
                .catch(e => {
                    console.log(e);
                    this.showMessage(e, "", "error");
                });
        },
        modificar: function() {
            console.log(this.TipoTransaccion);
            axios.put(this.baseUrl + this.TipoTransaccion.Id, {
                    id: this.TipoTransaccion.Id,
                    nombre: this.TipoTransaccion.Nombre,
                    descripcion: this.TipoTransaccion.Descripcion,
                    activo: this.TipoTransaccion.Activo
                })
                .then(response => {
                    // JSON responses are automatically parsed.
                    console.log(response.data)
                        //Notification
                    this.showMessage("Los datos se modificaron correctamente.", "", "success");

                })
                .catch(e => {
                    console.log(e);

                    this.showMessage(e, "", "error");
                });


        },
        validate: function() {
            this.$validator.validateAll();

            if (!this.errors.any()) {
                this.TipoTransaccion.Id != 0 ? this.modificar() : this.guardar();
            }
        },
        showMessage: function(titulo, mensaje, tipo) {
            //Notification
            swal({
                title: titulo,
                text: mensaje,
                icon: tipo
            });
        }
    },
    computed: {
        filteredList: function() {
            return this.TipoTransacciones.filter(function(item) {
                return this.searchKey == '' || (item.nombre.toLowerCase().indexOf(this.searchKey.toLowerCase()) !== -1) || (item.descripcion.toLowerCase().indexOf(this.searchKey.toLowerCase())) !== -1;
            }, this);
        }
    }

};

export default TipoTransaccion;