using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Repository;
using Service;

namespace InventarioApp.Controllers
{
    [Route("api/[controller]")]
    public class BaseController<TRepository, TDto, TEntity> : Controller
    where TRepository : IGenericService<TDto>
    where TDto : class
    where TEntity : BaseModel
    {

        IGenericService<TDto> _Service;
        ILogger _Logger;

        public BaseController(IGenericService<TDto> service, ILoggerFactory loggerFactory)
        {
            _Service = service;
            _Logger = loggerFactory.CreateLogger(nameof(BaseController<TRepository, TDto, TEntity>));
        }
        // GET api/TipoInventario
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var tiposInventario = await _Service.GetAllAsync();
                return Ok(tiposInventario);
            }
            catch (Exception exp)
            {
                _Logger.LogError(exp.Message);
                return BadRequest(exp);
            }

        }

        // GET api/TipoInventario/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var tipoInventario = await _Service.FindAsync(id);
                return Ok(tipoInventario);
            }
            catch (Exception exp)
            {
                _Logger.LogError(exp.Message);
                return BadRequest(exp);
            }
        }

        // POST api/TipoInventario
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]TDto tipoInventario)
        {
            try
            {
                var tipo = await _Service.SaveAsync(tipoInventario);
                return Ok(tipo);
            }
            catch (Exception exp)
            {
                _Logger.LogError(exp.Message);
                return BadRequest(exp);
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]TDto tipoInventario)
        {
            try
            {
                var tipo = await _Service.SaveAsync(tipoInventario);
                return Ok(tipo);
            }
            catch (Exception exp)
            {
                _Logger.LogError(exp.Message);
                return BadRequest(exp);
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var tipoInventario = await _Service.FindAsync(id);

                var tipo = await _Service.DeleteAsync(tipoInventario);
                return Ok(tipo);
            }
            catch (Exception exp)
            {
                _Logger.LogError(exp.Message);
                return BadRequest(exp);
            }
        }
    }
}
