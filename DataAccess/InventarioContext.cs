﻿using DataAccess.Mappings;
using DomainModel;
using Microsoft.EntityFrameworkCore;
using DomainModel.Base;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Threading;
using System.Threading.Tasks;

namespace DataAccess
{
    public class InventarioContext : DbContext
    {

        public InventarioContext(DbContextOptions options) : base(options)
        {
        }

        public virtual object GetKey(EntityEntry entity)
        {
            return entity.Entity.GetType().GetProperty("Id").GetValue(entity.Entity, null);
        }

        public override int SaveChanges()
        {
            TrackingChange();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            TrackingChange();
            return base.SaveChangesAsync(cancellationToken);
        }

        public void TrackingChange()
        {
            var modifiedEntities = ChangeTracker.Entries()
                .Where(p => p.State == EntityState.Modified).ToList();
            var now = DateTime.Now;

            foreach (var change in modifiedEntities)
            {
                var entityName = change.Entity.GetType().Name;
                var primaryKey = GetKey(change);

                foreach (var prop in change.OriginalValues.EntityType.GetProperties())
                {
                    if (prop == null) return;
                    var originalValue = change.OriginalValues[prop]?.ToString();
                    var currentValue = change.CurrentValues[prop]?.ToString();
                    if (originalValue != currentValue)
                    {
                        ChangeLog log = new ChangeLog()
                        {
                            EntityName = entityName,
                            PrimaryKeyValue = primaryKey.ToString(),
                            PropertyName = prop.Name,
                            OldValue = originalValue,
                            NewValue = currentValue,
                            DateChanged = now,
                            UserName = "System"
                        };
                        ChangeLogs.Add(log);
                    }
                }
            }

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new TipoInventarioMap());
            modelBuilder.ApplyConfiguration(new AlmacenMap());
            modelBuilder.ApplyConfiguration(new TipoTransaccionMap());
            modelBuilder.ApplyConfiguration(new ArticuloMap());
            modelBuilder.ApplyConfiguration(new TransaccionMap());
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<TipoInventario> TiposInventario { get; set; }
        public DbSet<ChangeLog> ChangeLogs { get; set; }
        public DbSet<Almacen> Almacenes { get; set; }
        public DbSet<TipoTransaccion> TiposTransaccion {get; set;}
        public DbSet<Articulo> Articulos {get; set;}
        public DbSet<Transaccion> Transacciones {get;set;}

    }
}
