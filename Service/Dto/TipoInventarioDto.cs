using System;
using System.Collections.Generic;
using System.Text;
using DomainModel;

namespace Service.Dto
{
    public class TipoInventarioDto : BaseModel
    {
        public string Descripcion { get; set; }
        public string CuentaContable { get; set; }
        public virtual ICollection<Articulo> Articulos { get; set; }
    }
}
