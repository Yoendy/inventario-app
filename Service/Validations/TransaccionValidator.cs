using Service.Dto;
using FluentValidation;

namespace Service.Validations
{
    public class TransaccionValidator : AbstractValidator<TransaccionDto>
    {
        public TransaccionValidator()
        {
            RuleFor(item => item.Descripcion).NotEmpty().WithMessage("La descripción es obligatoria.");

            RuleFor(item => item.Cantidad).NotEmpty().WithMessage("La cantidad es obligatoria.")
                .GreaterThan(0).WithMessage("La cantidad debe ser mayor que 0.");

            RuleFor(item => item.Monto).NotEmpty().WithMessage("El monto es obligatorio.")
                .GreaterThan(0).WithMessage("El monto debe ser mayor que 0.");

            RuleFor(item => item.TipoTransaccionId).NotEmpty().WithMessage("El tipo de transacción es obligatorio.");

            RuleFor(item => item.ArticuloId).NotEmpty().WithMessage("El articulo es obligatorio.");

            RuleFor(item => item.AlmacenId).NotEmpty().WithMessage("El almacén es obligatorio.");
        }
    }
}