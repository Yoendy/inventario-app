# Inventario App

Sistema de Inventario que cumpla las siguientes caracteristicas:

  - Gestión de Tipo Inventario
  - Gestión de Artículos
  - Gestión de Almacenes
  - Gestión de Tipo de Transacción
  - Gestión de Transacciones (Entrada, Salida, Ajustes)
  - Integración con el Sistema de Contabilidad
  - Consulta de existencias por artículos

## Getting Started

Estas instrucciones le conseguirán una copia del proyecto funcionando en su máquina local para propósitos de desarrollo y pruebas.

### Prerequisitos

Para iniciar este proyecto es necesario tener las siguientes herramientas:

  - .Net Core (2.1.2 o superior)
  - Node js (5.6.0 o superior)

### Installing

Aqui se describen los pasos para iniciar el proyecto en ambiente de desarrollo

#### WebService

```
dotnet restore
```

```
dotnet build
```

```
dotnet run --project InventarioApp
```

Por defecto esta configurado para iniciar en "http://localhost:5000"

#### WebClient

```
npm install
```

```
npm start
```

Por defecto esta configurado para iniciar en "http://localhost:8080"

## Tecnologias Utilizadas

* [.Net Core](https://docs.microsoft.com/en-us/dotnet/core/) - El framework del WebApi
* [Node.js](https://nodejs.org/es/) - El framework Web utilizado
* [Vue.js](https://vuejs.org/v2/guide/) - El framework Frontend (UI)
* [Webpack](https://webpack.js.org/guides/) -  Empaquetador de Módulos (Module Bundler)

## Autores

* **Yoendy F. Maldonado** - *Inicio de WebApi* - [Yoendy](https://bitbucket.com/yoendy)
* **Jorge Miguel Hernandez** - *Inicio de Cliente Web* - [Devhernandez](https://bitbucket.com/devhernandez)
* **Sergio Encarnacion** - *Diseño de Base de Datos* - [Sergio](https://bitbucket.com/)
* **Francisco Javier Buten** - *Integración con Contabilidad* - [Javier](https://bitbucket.com/)

## Arquitectura

* **DomainModel**: Entidades de negocios (Modelos del Dominio).
* **DataAcces**: Entity Framework (ORM), Validaciones de FluentApi.
* **Repository**: Manejar el Acceso a los Datos.
* **Service**: Incluir la logica del negocio, los dtos y validaciones de los dtos.
* **InventarioApp**: Esta el WebApi (Se manejan los controladores del sistema).
* **Inventario-Client**: Se maneja la parte visual del proyecto.

## Conocimientos

### Web API

* RestSharp: [RestSharp](http://restsharp.org/)
* FluentValidation: [FluentValidation](https://github.com/JeremySkinner/FluentValidation)
* AutoMapper: [AutoMapper](https://github.com/AutoMapper/AutoMapper)
* Fluent API: [Fluent API](https://msdn.microsoft.com/en-us/library/jj591617(v=vs.113).aspx)
* Entity Framework Core: [Entity Framework Core](https://docs.microsoft.com/en-us/ef/core/)
* Sqlite: [Sqlite](https://www.sqlite.org/docs.html)
* Sql Server: [ Sql Server](https://www.microsoft.com/es-es/sql-server/sql-server-2016)

### Web Client

* Webpack: [Webpack](https://webpack.js.org/concepts/)
* Vue.js: [Vue.js](https://vuejs.org/v2/guide/)
* Axios.js: [Axios](https://github.com/axios/axios)
* Vue-router: [Vue-router](https://router.vuejs.org/en/)
* VeeValidate: [VeeValidate](http://vee-validate.logaretm.com/)
* Pug: [Pug](https://pugjs.org/api/getting-started.html)
* Bootstrap: [Bootstrap](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
