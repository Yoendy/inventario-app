using DomainModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Mappings
{
    public class TipoTransaccionMap : IEntityTypeConfiguration<TipoTransaccion>
    {
        public void Configure(EntityTypeBuilder<TipoTransaccion> builder)
        {
            builder.Property(m => m.Descripcion).IsRequired();
            builder.Property(m => m.Nombre).IsRequired().HasMaxLength(150);
           
        }
    }
}
