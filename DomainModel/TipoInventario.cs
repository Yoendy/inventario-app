﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel
{
    public class TipoInventario : BaseModel
    {
        public string Descripcion { get; set; }
        public string CuentaContable { get; set; }
        public virtual  ICollection<Articulo> Articulos { get; set; }
    }
}
